package ticTacToe;

import java.util.Scanner;

class UI {
	private Scanner scanner;

	public String prompt(String promptMessage)
	{
		System.out.println(promptMessage);
		System.out.print(">");
		String userInput = scanner.nextLine();
		System.out.println("");
		return userInput;
	}

	public boolean validate(String userInput)
	{
		if(userInput.length() == 1) {
			for (int i = 1; i < 10; i++) {
				if (Character.getNumericValue(userInput.charAt(0)) == i) {
					return true;
				}
			}
		}
		return false;
	}

	public void printWelcomeMessage()
	{
		System.out.println("Welcome to ASCII Tic-Tac-Toe!");
		System.out.println("");
	}

	public void printThanksMessage()
	{
		System.out.println("Thanks for playing ASCII Tic-Tac-Toe!");
	}

	public void printGameOverMessage(GameResult gr, Player player1, Player player2)
	{
		switch (gr){
		case player1win:
			System.out.println("Congratulations, " + player1.getName() + " (" + player1.getMarker() + ")!");
			System.out.println("");
			break;
		case player2win:
			System.out.println("Congratulations, " + player2.getName() + " (" + player2.getMarker() + ")!");
			System.out.println("");
			break;
		case tie:
			System.out.println("Too bad; the game was a tie. Better luck next time!");
			System.out.println("");
			break;
		default:
			//serious error; write custom exception
		}
	}

	public UI()
	{
		this.scanner = new Scanner(System.in);
	}
}

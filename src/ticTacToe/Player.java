package ticTacToe;

public abstract class Player {
	private char marker;
	private String name;
	public abstract int getMove(UI ui, Board board);

	public char getMarker()
	{
		return marker;
	}

	public String getName() {
		return name;
	}

	public void setup(UI ui)
	{
		this.name = ui.prompt("What should this player's name be?");
		String reply = ui.prompt("Should " + this.getName() + " be 'X' or 'O'?");
		while (reply.length() != 1 || Character.toUpperCase(reply.charAt(0)) != 'X' && Character.toUpperCase(reply.charAt(0)) != 'O') {
			reply = ui.prompt("Please enter only either 'X' or 'O'.");
		}
		this.marker = reply.toUpperCase().charAt(0);
	}

	public Player(UI ui)
	{
		this.setup(ui);
	}
}

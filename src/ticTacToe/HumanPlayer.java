package ticTacToe;

class HumanPlayer extends Player {
	@Override
	public int getMove(UI ui, Board board)
	{
		System.out.println(this.getName() + "'s move!");
		board.print();
		String userInput = ui.prompt("Where would you like to go?");
		//validate userInput and parse to int;
		int chosenMove = 0;
		while (!ui.validate(userInput)){
			userInput = ui.prompt("Please enter a number 1-9!");
			chosenMove = Character.getNumericValue(userInput.charAt(0));
			System.out.print(chosenMove);
		}
		chosenMove = Character.getNumericValue(userInput.charAt(0));
		return chosenMove;
	}

	public HumanPlayer(UI ui)
	{
		super(ui);
	}

}

package ticTacToe;

class Board {
	//represents game board with [i][j] representing over i, down j
	private char[][] gameBoard;

	//prints the current game board
	public void print()
	{
		System.out.printf("     %c || %c || %c \n", this.get(1), this.get(2), this.get(3));
		System.out.println("    =============");
		System.out.printf("     %c || %c || %c \n", this.get(4), this.get(5), this.get(6));
		System.out.println("    =============");
		System.out.printf("     %c || %c || %c \n", this.get(7), this.get(8), this.get(9));
	}

	//this should set a char in the given location
	public void set(char c, int location)
	{
		switch(location) {
		case 1:
			gameBoard[0][0] = c;
			break;
		case 2:
			gameBoard[1][0] = c;
			break;
		case 3:
			gameBoard[2][0] = c;
			break;
		case 4:
			gameBoard[0][1] = c;
			break;
		case 5:
			gameBoard[1][1] = c;
			break;
		case 6:
			gameBoard[2][1] = c;
			break;
		case 7:
			gameBoard[0][2] = c;
			break;
		case 8:
			gameBoard[1][2] = c;
			break;
		case 9:
			gameBoard[2][2] = c;
			break;
		default:
			System.out.println("This is impossible!");
			break;
		}
	}

	//returns the char at the given location
	public int get(int location)
	{
		switch(location) {
		case 1:
			return gameBoard[0][0];
		case 2:
			return gameBoard[1][0];
		case 3:
			return gameBoard[2][0];
		case 4:
			return gameBoard[0][1];
		case 5:
			return gameBoard[1][1];
		case 6:
			return gameBoard[2][1];
		case 7:
			return gameBoard[0][2];
		case 8:
			return gameBoard[1][2];
		case 9:
			return gameBoard[2][2];
		default:
			//this should never happen
			System.out.println("You have chosen an incorrect location!");
			System.out.println("Please report this error to the developer!");
			return 'Q';
		}

	}
	
	public boolean isVacant(int location) {
		if (this.get(location) != 'X' && this.get(location) != 'O') {
			return true;
		} else {
			return false;
		}
	}

	public Boolean gameOver()
	{
		//should check if game is over and return
		if (checkForFilledBoard()) {
			return true;
		}

		if (checkForWinner('O')) {
			return true;
		}

		if (checkForWinner('X')) {
			return true;
		}

		return false;
	}

	public Boolean checkForFilledBoard()
	{
		for(char[] charArray:gameBoard) {
			for(char c:charArray){
				if(c != 'X' && c != 'O') {
					return false;
				}
			}
		}
		return true;
	}

	//analyzes the board nand returns a GameResult
	public GameResult getResultOfGame(char player1marker, char player2marker)
	{
		char winningChar = 'Q';
		//did x win?
		if(this.checkForWinner('X')) {
			winningChar = 'X';
		}
		//did o win?
		if(this.checkForWinner('O')) {
			winningChar = 'O';
		}
		if(player1marker == winningChar) {
			return GameResult.player1win;
		} else {
			if(player2marker == winningChar) {
				return GameResult.player2win;
			} else {
			return GameResult.tie;
			}
		}

	}

	//checks if the given char won
	private Boolean checkForWinner(char c)
	{
		//check each row
		for(int i = 1; i < 3; ++i) {
			if(this.checkRow(i, c)) {
				return true;
			}
		}

		//check each column
		for(int i = 1; i < 3; ++i) {
			if(this.checkColumn(i, c)) {
				return true;
			}
		}

		//check the diagonals
		if(checkDiagonals(c)) {
			return true;
		}

		//if nothing panned out, return false
		return false;

	}

	//check if a row is a winner for the given checkChar
	private Boolean checkRow(int rowNumber, char checkChar)
	{
		if( gameBoard[0][rowNumber - 1] == checkChar &&
			gameBoard[1][rowNumber - 1] == checkChar &&
			gameBoard[2][rowNumber - 1] == checkChar) {
			return true;
		} else {
			return false;
		}
	}

	//check if a column is a winner for the given checkChar
	private Boolean checkColumn(int columnNumber, char checkChar)
	{
		if( gameBoard[columnNumber - 1][0] == checkChar &&
			gameBoard[columnNumber - 1][1] == checkChar &&
			gameBoard[columnNumber - 1][2] == checkChar) {
			return true;
		} else {
			return false;
		}
	}

	//check if a diagonal is a winner for the given checkChar
	private Boolean checkDiagonals(char checkChar)
	{
		if( get(1) == checkChar && get(5) == checkChar && get(9) == checkChar ||
			get(3) == checkChar && get(5) == checkChar && get(7) == checkChar ) {
			return true;
		} else {
			return false;
		}
	}


	//initializes board
	public Board()
	{
		gameBoard = new char[3][3];
		gameBoard[0][0] = '1';
		gameBoard[1][0] = '2';
		gameBoard[2][0] = '3';
		gameBoard[0][1] = '4';
		gameBoard[1][1] = '5';
		gameBoard[2][1] = '6';
		gameBoard[0][2] = '7';
		gameBoard[1][2] = '8';
		gameBoard[2][2] = '9';
	}
}

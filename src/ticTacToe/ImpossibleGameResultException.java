package ticTacToe;

public class ImpossibleGameResultException extends Exception {
	private static final long serialVersionUID = 2850169319474336518L;

	public ImpossibleGameResultException(String message) {
		super(message);
		
	}
}

package ticTacToe;

public class Game {
	public static void main(String[] args){
		Board board = new Board();
		UI ui = new UI();

		//basic game menu + options
		//adjust as needed

		ui.printWelcomeMessage();
		
		//get the desired markers for each user
		
		//get the desired marker for each player and initialize Players
		Player player1 = new HumanPlayer(ui);
		Player player2 = new HumanPlayer(ui);

		//game starts
		//**begin primary game loop**

		//checks if the game is over
		while (!board.gameOver()) {

			//gets player1's move and makes it
			//we need to check if the square is already occupied here
			int player1Move = player1.getMove(ui, board);
			while (!board.isVacant(player1Move)) {
				System.out.println("Try again! That space is already taken!");
				player1Move = player1.getMove(ui, board);
			}
			board.set(player1.getMarker(), player1Move);

			//gets player2's move and makes it
			//we need to check if the square is already occupied here
			int player2Move = player2.getMove(ui, board);
			while (!board.isVacant(player2Move)) {
				System.out.println("Try again! That space is already taken!");
				player2Move = player2.getMove(ui, board);
			}
			board.set(player2.getMarker(), player2Move);
		}

		//**end primary game loop**

		//here should be where the winner is determined and the game ends
		GameResult gr = board.getResultOfGame(player1.getMarker(), player2.getMarker());

		ui.printGameOverMessage(gr, player1, player2);

		ui.printThanksMessage();
	}
}

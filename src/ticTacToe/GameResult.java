package ticTacToe;

public enum GameResult {
	player1win, player2win, tie
}
